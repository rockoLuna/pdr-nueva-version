import axios from 'axios';

export default(servidor) => {
    
    return axios.create({
        baseURL: servidor + "/rings-api/",

        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
}